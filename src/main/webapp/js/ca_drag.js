// JavaScript Document
(function(){
	$.fn.dragObj = function(options){
		dft		=	{
			obj		: this,					//当前拖动对象
			disX	: 0,					//获取鼠标第一次点击对象的x坐标
			disY	: 0,					//获取鼠标第一次点击对象的y坐标
			w		: $(window).width(),	//可以拖动的范围的宽度
			h		: $(window).height()	//可以拖动的范围的高度
		}

		var ops = $.extend(dft,options);
		var ele = $(ops.obj);
		
		$(ops.obj).mousedown(function(event){
			
			var event = event || window.event;
			ops.disX =  event.clientX - $(ops.obj).position().left;
			ops.disY =  event.clientY - $(ops.obj).position().top;
			
			$(document).mousemove(function(event){
				
				var event 	= event || window.event;
				var iL 		= event.clientX - ops.disX;
				var iT		= event.clientY - ops.disY;
				var border_w= parseInt($(ops.obj).css("border-width"))*2; //边框宽度
				var maxL 	= ops.w - $(ops.obj).width()-border_w;
				var maxT 	= ops.h - $(ops.obj).height()-border_w;
				
				iL <= 0 && (iL = 0);
				iT <= 0 && (iT = 0);
				iL >= maxL && (iL = maxL);
				iT >= maxT && (iT = maxT);
				
				$(ops.obj).css({left:iL,top:iT});
				return false;
			});
			
			$(document).mouseup(function(){
				$(document).unbind("mousemove");
				$(document).unbind("mouseup");
				
				ele[0].releaseCapture && ele[0].releaseCapture()
			});
			ele[0].setCapture && ele[0].setCapture();		
			return false
		}); 
	}
})(jQuery);
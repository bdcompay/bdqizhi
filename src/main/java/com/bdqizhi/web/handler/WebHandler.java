package com.bdqizhi.web.handler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebHandler {

	@RequestMapping(value="index")
	public ModelAndView index(){
		return new ModelAndView("index");
	}
	
	@RequestMapping(value="info1_list.html")
	public ModelAndView info_list(){
		return new ModelAndView("info1_list");
	}
	
	@RequestMapping(value="article.html")
	public ModelAndView article(){
		return new ModelAndView("article");
	}
	
	@RequestMapping(value="product_list.html")
	public ModelAndView product(){
		return new ModelAndView("product_list");
	}
	
	@RequestMapping(value="product2_list.html")
	public ModelAndView product2(){
		return new ModelAndView("product2_list");
	}
	
	@RequestMapping(value="product3_list.html")
	public ModelAndView product3(){
		return new ModelAndView("product3_list");
	}
}
package com.bdqizhi.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 请求编码过滤
 * @author WF
 *
 */
public class EncodingFilter implements Filter{
	
	/**
	 * 配置中默认的字符编码
	 */
	protected String encoding = null;
	protected FilterConfig filterConfig;
	/**
	 * 当没有指定默认编码时是否允许跳过过滤
	 */
	protected boolean ignore = true;
	
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		this.filterConfig = filterConfig;
		this.encoding = filterConfig.getInitParameter("encoding");
		String value = filterConfig.getInitParameter("ignore");
		if (value == null) {
			this.ignore = true;
		} else if (value.equalsIgnoreCase("true")) {
			this.ignore = true;
		} else if (value.equalsIgnoreCase("yes")) {
			this.ignore = true;
		} else {
			this.ignore = false;
		}
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		// TODO Auto-generated method stub
		HttpServletRequest hRequest = (HttpServletRequest) request;
		HttpServletResponse hResponse = (HttpServletResponse) response;
		
		if(ignore||hRequest.getCharacterEncoding()==null){
			String coding = selectEncoding();
			if(coding!=null){
				hRequest.setCharacterEncoding(coding);
				hResponse.setCharacterEncoding(coding);
			}
		}
		chain.doFilter(hRequest, hResponse);
	}


	public void destroy() {
		// TODO Auto-generated method stub
		this.encoding = null;
		this.filterConfig = null;
	}
	
	protected String selectEncoding(){
		return this.encoding;
	}

}
